FROM adoptopenjdk/openjdk11:latest

RUN apt-get update -qq && apt-get upgrade -qq && apt-get install -qq jq \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY target/csql-*.jar /app/csql.jar

ENTRYPOINT ["java", "-jar", "/app/csql.jar"]
