package io.truemark.csql

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Options
import org.yaml.snakeyaml.Yaml
import java.io.FileInputStream
import java.sql.Connection
import java.sql.DriverManager
import kotlin.system.exitProcess

fun eprintln(string: String) = System.err.println("Error: %s".format(string))

enum class DatabaseType {
  oracle, postgres
}

data class Database(
    var type: DatabaseType? = null,
    var hostname: String? = null,
    var service: String? = null,
    var port: Int? = null,
    var username: String? = null,
    var password: String? = null
)

data class Config(
    var databases: List<Database>? = null
)

fun getConnectionString(database: Database): String {
  val type = database.type
  val str = when(type) {
    DatabaseType.oracle -> "jdbc:oracle:thin:@//%s:%d/%s"
    DatabaseType.postgres -> "jdbc:postgresql://%s:%d/postgres"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  return str.format(
      database.hostname,
      database.port,
      database.service)
}

fun getConnection(database: Database): Connection {
  val constr = getConnectionString(database)
  println("Using connection string %s".format(constr))
  val con = DriverManager.getConnection(constr,
      database.username,
      database.password)
  con.autoCommit = false
  return con
}

fun testConnection(database: Database, con: Connection) {
  println("Testing database connectivity")
  val type = database.type
  val query = when(type) {
    DatabaseType.oracle -> "select 1 from dual"
    DatabaseType.postgres -> "select 1"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    createStatement().use {stmt -> stmt.execute(query)}
  }
  println("Successfully connected to database")
}

fun validateConfig(config: Config) {
  validateDatabases(config)
}

fun validateDatabases(config: Config) {
  if (config.databases.isNullOrEmpty()) {
    eprintln("One or more databases is required.")
    exitProcess(0)
  }
  else {
    for (db in config.databases!!) {
      validateDatabase(db)
    }
  }
}

fun validateDatabase(database: Database?) {
  if (database?.type == null) {
    eprintln("Missing database type")
    exitProcess(1)
  }
  if (database.hostname.isNullOrBlank()) {
    eprintln("Missing database hostname")
    exitProcess(1)
  }
  if (database.type == DatabaseType.oracle) {
    if (database.service.isNullOrBlank()) {
      eprintln("Missing database service")
      exitProcess(1)
    }
  }
  if (database.port == null) {
    eprintln("Missing database port")
    exitProcess(1)
  }
  if (database.username.isNullOrBlank()) {
    eprintln("Missing database username")
    exitProcess(1)
  }
  if (database.password.isNullOrBlank()) {
    eprintln("Missing database password")
    exitProcess(1)
  }
}

suspend fun queryWorker(database: Database, query: String) {
  withContext(Dispatchers.IO) {
    println("Running query: " + query + " for database:" + database.hostname)
    getConnection(database).use { con ->
      testConnection(database, con)
      with(con) {
        prepareStatement(query.format()).use { stmt ->
          stmt.executeQuery().use { rs ->
            rs.next()
            rs.getString(0)
          }
        }
      }
    }
  }
}

suspend fun main(args: Array<String>) {

  val options = Options()
  options.addOption("h", "help", false, "print help menu")
  options.addOption("p", "password", true, "database password override")

  val parser = DefaultParser()
  val cmd = parser.parse(options, args)

  if (cmd.hasOption("help")) {
    val formatter = HelpFormatter()
    formatter.printHelp("csql [OPTIONS] <FILE> <QUERY> [USERNAME] [PASSWORD] ", options)
    exitProcess(0)
  }

  var dbpass: String? = null
  if (cmd.hasOption("password")) {
    dbpass = cmd.getOptionValue("password")
  }

  val remainingArgs = cmd.argList
  if (remainingArgs.size < 1) {
    eprintln("Config file required as first parameter")
    exitProcess(1)
  } else if (remainingArgs.size == 1) {
    eprintln("A query is required as a second parameter")
    exitProcess(1)
  }

  val filename = remainingArgs[0]
  val query = remainingArgs[1]

  // Load configuration
  println("Reading configuration from %s".format(filename))
  val config = FileInputStream(filename).use { Yaml().loadAs(it, Config::class.java) }

  validateConfig(config)
  println("Validation complete.")

  for (database in config?.databases!!) {
    if (dbpass != null) {
      println("Applying password override for: " + database.hostname)
      database.password = dbpass
    }
    queryWorker(database, query)
  }
}

